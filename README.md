# Jarris PC Info #

I used a modification of conkyrc_seamod by SeaJeyfor the base, then added more information in general

##What does it report?##

OS + Kernal Version
Username + hostname
Uptime

CPU Usage total, plus top 5 loads.

RAM Usage total, plus top 5 loads.

Disk usage, comes preconfigured with Root, Drive1, Drive2 (You must edit this).

Network usage, plus your internal IP and a graph.

Teperature and fans for Nvidia GPU + CPU (you may have to edit this).

VLC Information regarding currently playing song (however no controls)

## Screenshot ##
![screenshot.png](https://bitbucket.org/repo/GKbdxy/images/31037908-screenshot.png)

## Installation ##

If you do not have Conky installed, run this command in a terminal

```
#!shell

sudo apt-get install conky-all
```
Then run this to create the folder, then clone the files into it

```
#!shell

mkdir -p ~/.conky/JarrisPCInfo
git clone git@bitbucket.org:DarkJarris/jarrisseamod.git ~/.conky/JarrisPCInfo
```

Notes: 
This assumes ~/.conky/JarrisPCInfo. If not, correct script references in conkyrc_pcinfo file. Lines 118-124.  
You may need to edit the networking section to properly pick up your network usage. Lines 100-104.  
You will need to change the mounted drives section to properly reflect your drives. Lines 93-97.


##Setting up VLC to display Information##

Open VLC, and open *Tools->Preferences->Show Settings All (At the bottom)*
On the left *Interface->Main Interfaces*

Tick Web

expand *Main Interfaces->Lua
*
Create a Lua HTTP Password of your choosing.

Open the /JarrisPCInfo/VLCConkyInt Python script in your preffered text editor

edit line 10 and change "PASSWORD" to the password you chose in VLC

*data = check_output(["curl", "-s", "-u", ":PASSWORD", "http://127.0.0.1:8080/requests/status.xml"], shell=False)*



# Starting at Boot #

To make it autostart, you can use either of 2 methods:

### Crontab ###
```
#!shell


crontab -e
```

Adding a line like this to it:


```
#!shell

@reboot conky -c ~/.conky/JarrisPCInfo/conkyrc_pcinfo
```

will execute the script once your computer boots up.


### GUI ###

System->Preferences->Startup Applications->Add

*Refer to your Desktop Environment equivalent if you cannot see it*

```
#!shell

conky -c ~/.conky/JarrisPCInfo/conkyrc_pcinfo
```